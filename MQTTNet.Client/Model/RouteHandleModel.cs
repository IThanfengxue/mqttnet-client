﻿using MQTTnet.Protocol;

using MQTTNet.Client.Enums;

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace MQTTNet.Client.Model
{
    public class RouteHandleModel
    {
        public string? Topic { get; set; }
        public int Identity { get; set; }
        public QOSEnum QOS { get; set; }
        public MethodInfo? Method { get; set; }
        public string? NameSpace { get; set; }
        public string? ClassName { get; set; }
    }
}
