﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTNet.Client.Model
{
    public class ReceivedModel
    {
        public string? Topic { get; set; } 
        public byte[]? Payload { get; set; }
        public string? Content { get; set; }
        public IMQTTNetClient Client { get; set; }
    }
}
