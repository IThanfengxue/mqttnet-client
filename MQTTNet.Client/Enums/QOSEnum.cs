﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTNet.Client.Enums
{
    public enum QOSEnum
    {
        AtMostOnce,
        AtLeastOnce,
        ExactlyOnce
    }
}
