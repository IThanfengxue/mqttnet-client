﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTNet.Client.Enums
{
    public enum MQTTVersionEnum
    {
        Unknown = 0,
        V310 = 3,
        V311 = 4,
        V500 = 5
    }
}
