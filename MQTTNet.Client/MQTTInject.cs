﻿//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Hosting;

//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace MQTTNet.Client
//{
//    public static class MQTTInject
//    {
//        public static IServiceCollection AddMQTTNetClient(this IServiceCollection services)
//        {
//            services.AddSingleton<IMQTTNetClient, MQTTNetClient>();
//            return services;
//        }
//        public static IServiceCollection AddMQTTNetClient(this IServiceCollection services, string uri, int port = 1883, string UserName = "", string Password = "", string clientId = "")
//        {
//            services.AddSingleton<IMQTTNetClient, MQTTNetClient>();
//            IMQTTNetClient? inc = services.BuildServiceProvider().GetService<IMQTTNetClient>();
//            inc?.InitMQTTClient(uri, port, UserName, Password, clientId);
//            inc?.Connection();
//            return services;
//        }
//    }
//}
