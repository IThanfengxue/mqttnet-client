﻿using MQTTnet;
using MQTTnet.Formatter;
using MQTTNet.Client.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTNet.Client.Common
{
    public static class MqttClientOptionsBuilderHelper
    {
        public static MqttClientOptions CrateMqttClientOptions(this MqttClientOptionsBuilder option,
            string Server, int Port, string User = "", string PassWd = "",
            string ClientId = "", MQTTVersionEnum MQTTVersion = MQTTVersionEnum.V500)
        {
            MqttProtocolVersion version = (MqttProtocolVersion)MQTTVersion;
            if (string.IsNullOrEmpty(ClientId))
                ClientId = Guid.NewGuid().ToString("N").ToUpper();
            option = option.WithProtocolVersion(version)
                .WithClientId(ClientId)
                .WithTcpServer(Server, Port)
                .WithCleanStart()
                .WithCleanSession()
                .WithTimeout(TimeSpan.FromSeconds(3))
                .WithKeepAlivePeriod(TimeSpan.FromSeconds(120));
            if (!string.IsNullOrEmpty(User) && !string.IsNullOrEmpty(PassWd))
            {
                option.WithCredentials(User, PassWd);
            }
            return option.Build();
        }
    }
}
