﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

using MQTTNet.Client.Common;
using MQTTNet.Client.Enums;

namespace MQTTNet.Client.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class RouterAttribute:Attribute
    {
        internal string Topic { get; private set; }
        protected string _topic { get; private set; }
        internal int _identity { get; private set; }
        public QOSEnum QOS { get; private set; }
        public RouterAttribute(string _Topic, QOSEnum _qos = QOSEnum.ExactlyOnce, int _identity = 0)
        { 
            this.Topic = _Topic;
            this.QOS = _qos;
            this._identity = _identity;
            this._topic= _Topic.ReplaceAll();
        }
    }
}
