﻿using MQTTNet.Client.Enums;
using MQTTNet.Client.Model;

using System;
using System.Collections.Generic;
using System.Text;

namespace MQTTNet.Client.Handles
{
    internal interface IRouteHandle
    {
        List<RouteHandleModel> Routes { get; }
        void Init(int identity = 0);
        bool AddRoute(string Topic,string Method,Type type,QOSEnum qos=QOSEnum.ExactlyOnce);
        bool ActionRoute(string Topic, byte[] Payload, IMQTTNetClient client);
    }
}
