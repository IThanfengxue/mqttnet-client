using example;

using MQTTNet.Client;

var builder = Host.CreateApplicationBuilder(args);
var service = builder.Services;
service.AddHostedService<Worker>();

var host = builder.Build();
host.Run();
