﻿using MQTTNet.Client.Attributes;
using MQTTNet.Client.Model;
using MQTTNet.Client.Common;
using MQTTNet.Client.Enums;
using MQTTNet.Client;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace example
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            IMQTTNetClient client = new MQTTNet.Client.MQTTNetClient("127.0.0.1", 1883, UserName: "admspay", Password: "admscarpays", Identity: 2);
            if (await client.Connection(s => {
                s.OnConnectionHandle = async (e) =>
                {
                    _logger.LogInformation("连接成功 Id：" + e);
                };
                s.OnDisConnectionHandle = async (e) =>
                {
                    _logger.LogInformation("连接断开 Id：" + e);
                };
            }))
                _logger.LogInformation("连接成功");
            _logger.LogInformation("成功");
            //IMQTTNetClient clients = new MQTTNet.Client.MQTTNetClient("127.0.0.1", 9900, UserName: "", Password: "",Identity: 0);
            //await clients.Connection();
           
        }
    }
    public class MQTTData : ReceivedModel {
        [Router("+/device/message/up/ivs_result", _identity:2 ,_qos:QOSEnum.ExactlyOnce)]
        public async Task GetData(ZSAlarmInfoPlateModelR3<ZSAlarmInfoPlateModel> resultR3)
        {
            if (resultR3 == null)
                return;
            Console.WriteLine($@"a：{resultR3.ObjectToJson()}");
            Console.WriteLine($@"主题：{this.Topic}");
            Console.WriteLine($@"原始字符串：{this.Content}");
            await Task.CompletedTask;
        }
        [Router("+/Synchronous/VehicleRegist",_identity: 2, _qos: MQTTNet.Client.Enums.QOSEnum.ExactlyOnce)]
        public async Task Env_VehicleRegistMQTT(ReceeivedProgramdata<object> obj)
        {
           this.Client.Publish(obj.ReTopic, new
            {
                Tag = 0,
                Message = "123321",
                Description = string.Empty,
                Total = 0
                
            }.ObjectToJson());
        }

    }
    public class ReceeivedProgramdata<T>
    {
        public string ProgramCode { get; set; }
        public string ReTopic { get; set; }
        public T Data { get; set; }
    }
    public class test
    {
        public Guid strc { get; set; }
        public string? strs { get; set; }
        public string? stra { get; set; }
        public List<test1> array { get; set; }
    }
    public class test1
    {
        public string? strs { get; set; }
        public string? stra { get; set; }
    }
    public class ZSAlarmInfoPlateModel
    {
        public ZSPlateResults AlarmInfoPlate { get; set; }
    }
    public class ZSAlarmInfoPlateModelR3<T>
    {
        public string id { get; set; }
        public long bv { get; set; }
        public string sn { get; set; }
        public string name { get; set; }
        public string version { get; set; }
        public long timestamp { get; set; }
        public long code { get; set; }
        public T payload { get; set; }
    }
    /// <summary>
    /// 识别结果
    /// </summary>
    public class ZSPlateResults
    {
        /// <summary>
        ///默认通道号
        /// </summary>
        public int channel { get; set; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string deviceName { get; set; }
        /// <summary>
        /// 设备IP
        /// </summary>
        public string ipaddr { get; set; }
        /// <summary>
        /// 数据回调
        /// </summary>
        public ZSPlateResultAlls result { get; set; }
        /// <summary>
        /// 设备序列号， 设备唯一
        /// </summary>
        public string serialno { get; set; }
        public string user_data { get; set; }
        public int heartbeat { get; set; }
    }

    public class ZSPlateResultAlls
    {
        public ZSPlateResultALL PlateResult { get; set; }
    }
    public class ZSPlateResultALL
    {
        /// <summary>
        /// 车牌真实宽度
        /// </summary>
        public int plate_true_width { get; set; }
        /// <summary>
        /// 车牌距离
        /// </summary>
        public int plate_distance { get; set; }
        /// <summary>
        /// 是否为伪车牌
        /// </summary>
        public int is_fake_plate { get; set; }
        /// <summary>
        /// 车辆品牌
        /// </summary>
        public CarBrand car_brand { get; set; }
        /// <summary>
        /// 亮度
        /// </summary>
        public int bright { get; set; }
        /// <summary>
        /// 车身亮度
        /// </summary>
        public int carBright { get; set; }
        /// <summary>
        /// 车辆颜色
        /// </summary>
        public int carColor { get; set; }
        /// <summary>
        /// 车牌颜色 0： 未知、 1： 蓝色、 2： 黄色、 3： 白色、 4： 黑色、 5： 绿色
        /// </summary>
        public int colorType { get; set; }
        public int colorValue { get; set; }
        public int confidence { get; set; }
        public int direction { get; set; }
        /// <summary>
        /// 大图bsae64
        /// </summary>
        public string imageFile { get; set; }
        public string full_image_content { get; set; }
        /// <summary>
        /// 长度
        /// </summary>
        public int imageFileLen { get; set; }
        /// <summary>
        /// 小图base64
        /// </summary>
        public string imageFragmentFile { get; set; }
        /// <summary>
        /// 小图长度
        /// </summary>
        public int imageFragmentFileLen { get; set; }
        /// <summary>
        /// 设备离线状态， 0： 在线， 1： 离线
        /// </summary>
        public int isoffline { get; set; }
        /// <summary>
        /// 车牌号
        /// </summary>
        public string license { get; set; }
        /// <summary>
        /// 车牌位置
        /// </summary>
        public ZSCarNumLocation location { get; set; }
        /// <summary>
        /// 车牌位置
        /// </summary>
        public ZSCarNumLocation car_location { get; set; }
        /// <summary>
        /// 识别的车牌Id
        /// </summary>
        public int plateid { get; set; }
        /// <summary>
        /// 识别时间
        /// </summary>
        public ZSAlarmDateTime timeStamp { get; set; }
        /// <summary>
        /// 识别所用时间
        /// </summary>
        public int timeUsed { get; set; }
        /// <summary>
        /// 当前结果的触发类型： 1： 自动触发类型、 2： 外部输入触
        ///发（IO 输入） 、 4： 软件触发（SDK） 、 8： 虚拟线圈触发
        /// </summary>
        public int triggerType { get; set; }
        /// <summary>
        /// 车辆类型
        /// </summary>
        public int type { get; set; }
    }

    public class CarBrand
    {
        public int brand { get; set; }
        public int year { get; set; }
        public int type { get; set; }
    }
    /// <summary>
    /// 车牌位置
    /// </summary>
    public class ZSCarNumLocation
    {
        public ZSLocation RECT { get; set; }
    }
    /// <summary>
    /// 车牌位置
    /// </summary>
    public class ZSLocation
    {
        /// <summary>
        /// 下边距
        /// </summary>
        public int bottom { get; set; }
        /// <summary>
        /// 左边距
        /// </summary>
        public int left { get; set; }
        /// <summary>
        /// 右边距
        /// </summary>
        public int right { get; set; }
        /// <summary>
        /// 上边距
        /// </summary>
        public int top { get; set; }
    }
    public class ZSAlarmDateTime
    {
        public ZSAlarmTime Timeval { get; set; }

    }
    /// <summary>
    /// 识别时间
    /// </summary>
    public class ZSAlarmTime
    {
        /// <summary>
        /// 日期
        /// </summary>
        public int decday { get; set; }
        /// <summary>
        /// 时
        /// </summary>
        public int dechour { get; set; }
        /// <summary>
        /// 分
        /// </summary>
        public int decmin { get; set; }
        /// <summary>
        /// 月
        /// </summary>
        public int decmon { get; set; }
        /// <summary>
        /// 秒
        /// </summary>
        public int decsec { get; set; }
        /// <summary>
        /// 年
        /// </summary>
        public int decyear { get; set; }
        /// <summary>
        /// 时间戳
        /// </summary>
        //public int sec { get; set; }
        /// <summary>
        /// 识别所用时间
        /// </summary>
        //public int usec { get; set; }

    }
}
